var express = require('express')
var router = express.Router()

router.use(express.static(__dirname + '/../assets'))

router.get('/', function (req, res) {
  
  var path = require('path');
  res.sendFile(path.resolve(__dirname +'/../index.html'));
})

module.exports = router